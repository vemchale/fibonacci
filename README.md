# fizzbuzz-fancy

This builds using either stack or cabal. It also includes a [justfile](https://github.com/casey/just)
which can be used to run `hlint`, `weeder`, and [`yamllint`](https://github.com/adrienverge/yamllint).

To see it in action:

```bash
 $ cabal new-test
 $ cabal new-bench
```

or

```bash
 $ stack test
 $ stack bench
```
