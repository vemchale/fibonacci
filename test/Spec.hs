import           FizzBuzz
import           Test.Hspec

main :: IO ()
main = hspec $ do
    describe "fib" $
        parallel $ it "gives the Fibonacci numbers in reverse order" $
            fib 6 `shouldBe` [1, 1, 2, 3, 5, 8]
    describe "fizzBuzz" $
        parallel $ it "should do the right thing for 1 through 15" $
            fizzBuzz 15 `shouldBe` ["1","1","BuzzFizz","Buzz","Fizz","8","BuzzFizz","Buzz","34","Fizz","BuzzFizz","Buzz","BuzzFizz","377","Fizz"]
