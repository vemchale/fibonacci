module Main where

import           Criterion.Main
import           FizzBuzz

main :: IO ()
main =
    defaultMain [ bgroup "fizzBuzz"
                      [ bench "fizzBuzz" $ nf fizzBuzz 20 ]
                , bgroup "fib"
                      [ bench "fib"  $ nf fib 20
                      , bench "fib'" $ nf fib' 19 ]
                ]
