module FizzBuzz
    ( fizzBuzz
    , fib
    , fib'
    ) where

import           Data.Functor.Foldable (para)
import           GHC.Natural           (Natural)

fib :: Natural -> [Natural]
fib n = take (fromIntegral n) fibs

fibs :: [Natural]
fibs = 1 : 1 : zipWith (+) (tail fibs) fibs

-- mostly just here to benchmark against. This was my initial attempt, but it is
-- around 4x slower than the other one.
fib' :: Natural -> [Natural]
fib' = para algebra where
    algebra (Just (_, past@(x:y:_))) = (x + y) : past
    algebra (Just (_, past@[x]))     = x : past
    algebra (Just (n, []))           = [n]
    algebra Nothing                  = [1]

isPrime :: Natural -> Bool
isPrime 1 = False
isPrime n = 0 `notElem` fmap (n `mod`) sqrtRange
    where sqrtRange = [2..(floor (sqrt (fromIntegral n :: Double)))]

fancy :: Natural -> String
fancy n
    | n `mod` 15 == 0 = "FizzBuzz"
    | n `mod` 5 == 0 = "Fizz"
    | n `mod` 3 == 0 = "Buzz"
    | isPrime n = "BuzzFizz"
    | otherwise = show n

fizzBuzz:: Natural -> [String]
fizzBuzz= fmap fancy . fib
