ci:
    @stack build --test --bench --no-run-tests --no-run-benchmarks
    hlint .
    weeder .
    @cabal new-build
    @cabal new-test
    @cabal new-bench
    yamllint stack.yaml
